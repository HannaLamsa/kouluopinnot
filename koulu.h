#ifndef KOULU_H_INCLUDED
#define KOULU_H_INCLUDED
#include "henkilo.h"
#include <vector>
#include <memory>
#include <ostream>
#include <string>

/**
    *  	\brief Luokka nimelta henkilo
*/
class henkilo;
/**
    *  	\brief Luokka nimelta opettaja
*/
class opettaja;
/**
    *  	\brief Luokka nimelta oppilas
*/
class oppilas;

/**
    *  	\brief Luokka nimelta koulu sisaltaa..
*/
class koulu{
/**
    *  	\brief Koulu on abstrakti luokka
*/

private:
    koulu();
    virtual ~koulu();
public:
    int henkilot;
    std::vector<henkilo*> henkilota;
    void lisaaOpettaja(std::string Etu, std::string Suku, std::string Aine);
    void lisaaOppilas(std::string Etu, std::string Suku, int Vuosi);
    void lisaaOpettajaolio(opettaja* o);
    void lisaaOppilasolio(oppilas* o);
    void print() const;
    /**
    *  	\brief henkilo voi hakea opettaja aliluokasta tietoa
    */
    friend class opettaja;
};

template<typename T>
void print(std::vector<T> container)
{
//toteutus tänne
    for(auto& tyyppi : container){
        std::cout << tyyppi->toString();
    }
}

#endif // KOULU_H_INCLUDED
