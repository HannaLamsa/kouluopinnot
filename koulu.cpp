#include "koulu.h"
#include "henkilo.h"
#include <sstream>
#include <vector>
#include <memory>
#include <ostream>
#include <string>

    /**
    *  	\brief Luo tyhjän kouluolion
    */
koulu::koulu(){
    henkilot = 0;
}
koulu::~koulu() {
    for(auto& tyyppi : henkilota) {
        delete tyyppi;
    }
}

void koulu::print() const {
    for(auto& tyyppi : henkilota){
        std::cout << tyyppi->toString();
    }

}

    /**
    *  	\brief Lisää kouluun opettajan
    */
void koulu::lisaaOpettaja(std::string Etu, std::string Suku, std::string Aine){
    opettaja* o1 = new opettaja(Etu,Suku,Aine);
    lisaaOpettajaolio(o1);
}

    /**
    *  	\brief Lisää kouluun oppilaan
    */
void koulu::lisaaOppilas(std::string Etu, std::string Suku, int Vuosi){
    oppilas* o1 = new oppilas(Etu,Suku,Vuosi);
    lisaaOppilasolio(o1);
}

    /**
    *  	\brief Lisää kouluun opettajan opettajaoliosta
    *  	\brief Opettajaolio o laitetaan opettajatvektorin perälle
    */
void koulu::lisaaOpettajaolio(opettaja* o){
    henkilota.push_back(o);
    henkilot = henkilot + 1;
}

    /**
    *  	\brief Lisää kouluun oppilaan oppilasoliosta
        \brief Oppilasolio o laitetaan oppilasvektorin perälle
    */
void koulu::lisaaOppilasolio(oppilas* o){
    henkilota.push_back(o);
    henkilot = henkilot + 1;
}

/*
template<typename T>
void print(std::vector<T> container)
{
    for(auto& tyyppi : container){
        std::cout << tyyppi->toString();
    }
}
*/
