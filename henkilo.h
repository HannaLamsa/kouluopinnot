#ifndef HENKILO_H_INCLUDED
#define HENKILO_H_INCLUDED
#include <string>
#include <iostream>
#include "koulu.h"

/**
    *  	\brief Luokka koulun jasenista
*/

class koulu;
/**
    *  	\brief Luokka henkioista
*/
class henkilo{
public:
/**
    *  	\brief A class for koulu members
*/
    virtual ~henkilo(){};
    virtual std::string toString() const = 0;
    virtual henkilo* clone() const = 0;

};
/**
    *  	\brief luokka opettaja on henkilön alaluokka
*/
class opettaja : public henkilo{
    std::string etunimi, sukunimi, aine;
public:
    opettaja();
    opettaja(std::string Etu, std::string Suku, std::string Aine);
    std::string getEtunimi() const;
    std::string getSukunimi() const;
    /**
    *   \brief Asetetaan Etunimi opettajalle
    *  	\param Etu asetetaan kirjaimilla
    */
    void setEtunimi(std::string Etu);
      /**
    *  	\brief Asetetaan Sukunimi opettajalle
    *  	\param Suku asetetaan kirjaimilla
    */
    void setSukunimi(std::string Suku);
    std::string getAine();
     /**
    *  	\brief Asetetaan opettama aine
    *  	\param Aine ilmoitetaan kirjaimilla
    */
    void setAine(std::string Ai);
    virtual std::string toString() const;
    virtual henkilo* clone() const;
    koulu* luoKoulu();
};

/**
    *  	\brief Luokka oppilas, joka on henkilö -luokan alaluokka
*/
class oppilas : public henkilo{
    std::string etunimi, sukunimi;
    int vuosi;
public:
    oppilas();
    oppilas(std::string Etu, std::string Suku, int Vuosi);
    std::string getEtunimi() const;
    std::string getSukunimi() const;
    /**
    *  	\brief Asetetaan Etunimi oppilaalle
    *  	\param Etu asetetaan kirjaimilla
    */
    void setEtunimi(std::string Etu);
     /**
    *  	\brief Asetetaan Sukunimi oppilaalle
    *  	\param Suku asetetaan kirjaimilla
    */
    void setSukunimi(std::string Suku);
     /**
    *  	\brief Asetetaan opintojen aloitusvuosi
    *  	\param Vuosi asetetaan numeroilla
    */
    int getVuosi();
    void setVuosi(int Vuosi);
    virtual std::string toString() const;
    virtual henkilo* clone() const;
};


#endif // HENKILO_H_INCLUDED
