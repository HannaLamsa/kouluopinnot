#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "henkilo.h"

TEST_CASE("Henkilotestit","Henkilotestit"){

    /**
    *  	\brief Luodaan opettaja ja oppilasolioita testejä varten
    */
    opettaja o1("Heikki","Ketonen","Matematiikka");
    opettaja o2("Vali", "Aikainen", "Nimi");
    oppilas o3("Ville","Kello",4);
    oppilas o4("Vali", "Aikainen", 0);
    /**
    *  	\brief Tarkistetaan opettajan o1 tiedot
    */
    CHECK(o1.getEtunimi() == "Heikki");
    CHECK(o1.getSukunimi() == "Ketonen");
    CHECK(o1.getAine() == "Matematiikka");

    /**
    *  	\brief Testataan vaihtaa opettajan o2 tietoja
    */
    o2.setEtunimi("Martta");
    o2.setSukunimi("Laitinen");
    o2.setAine("Biologia");

    /**
    *  	\brief Tarkistetaan menikö vaihdot oikein
    */
    CHECK(o2.getEtunimi() == "Martta");
    CHECK(o2.getSukunimi() == "Laitinen");
    CHECK(o2.getAine() == "Biologia");

    /**
    *  	\brief Tarkistetaan opiskelijan o3 tiedot
    */
    CHECK(o3.getEtunimi() == "Ville");
    CHECK(o3.getSukunimi() == "Kello");
    CHECK(o3.getVuosi() == 4);

    /**
    *  	\brief Testataan saadaanko syötettyä oppilaalle o4 tietoja
    */
    o4.setEtunimi("Kerttu");
    o4.setSukunimi("Pihlaja");
    o4.setVuosi(3);

    /**
    *  	\brief Testataan menikö tiedot oikein
    */
    CHECK(o4.getEtunimi() == "Kerttu");
    CHECK(o4.getSukunimi() == "Pihlaja");
    CHECK(o4.getVuosi() == 3);
}

