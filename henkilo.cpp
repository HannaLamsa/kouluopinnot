#include "henkilo.h"
#include "koulu.h"
#include <string>
#include <iostream>
#include <sstream>

    /**
    *  	\brief Opettajan tyhjä konstruktori
    */
opettaja::opettaja(){}

    /**
    *  	\brief Opettajan konstruktori jolla luodaan uusi opettajaolio
    */
opettaja::opettaja(std::string Etu, std::string Suku, std::string Ai){
    etunimi = Etu;
    sukunimi = Suku;
    aine = Ai;
}

    /**
    *  	\brief Palauttaa opettajan aineen
    */
std::string opettaja::getAine(){
    return aine;
}

    /**
    *  	\brief Asettaa opettajan aineen
    */
void opettaja::setAine(std::string Ai){
    aine = Ai;
}

    /**
    *  	\brief Palauttaa opettajan etunimen
    */
std::string opettaja::getEtunimi() const{
    return etunimi;
}

    /**
    *  	\brief Palauttaa opettajan sukunimen
    */
std::string opettaja::getSukunimi() const{
    return sukunimi;
}

    /**
    *  	\brief Asettaa opettajan etunimen
    */
void opettaja::setEtunimi(std::string Etu){
    etunimi = Etu;
}

    /**
    *  	\brief Asettaa opettajan sukunimen
    */
void opettaja::setSukunimi(std::string Suku){
    sukunimi = Suku;
}

    /**
    *  	\brief palauttaa opettajan tiedot yhtenä merkkijonona
    */
std::string opettaja::toString() const{
    std::string s = '\n' + "Etunimi: " + etunimi + '\n' + "Sukunimi: " + sukunimi + '\n' + "Aine: " + aine + '\n';
    return s;
}

    /**
    *  	\brief Oppilaan tyhjä konstruktori
    */
oppilas::oppilas(){}

    /**
    *  	\brief Oppilaan konstrukotir jolla luodaan oppilasolio
    */
oppilas::oppilas(std::string Etu, std::string Suku, int Vuosi){
    etunimi = Etu;
    sukunimi = Suku;
    vuosi = Vuosi;
}

    /**
    *  	\brief Palauttaa oppilaan etunimen
    */
std::string oppilas::getEtunimi() const{
    return etunimi;
}

    /**
    *  	\brief Palauttaa oppilaan sukunimen
    */
std::string oppilas::getSukunimi() const{
    return sukunimi;
}

    /**
    *  	\brief Asettaa oppilaan etunimeksi annetun nimen
    */
void oppilas::setEtunimi(std::string Etu){
    etunimi = Etu;
}

    /**
    *  	\brief Asettaa oppilaan sukunimeksi annetun sukunimen
    */
void oppilas::setSukunimi(std::string Suku){
    sukunimi = Suku;
}

    /**
    *  	\param Palauttaa oppilaan vuosikurssin
    */
int oppilas::getVuosi(){
    return vuosi;
}

    /**
    *  	\param Asettaa oppilaan vuosikurssin
    */
void oppilas::setVuosi(int Vuosi){

    if(Vuosi > 0){
        vuosi = Vuosi;
    }

}

    /**
    *  	\brief Palauttaa oppilaan tiedot yhtenä stringinä
    *   \return palauttaa oppilaan tiedot yhtenä merkkijonona
    */
std::string oppilas::toString() const{
    std::stringstream ss;
    ss << '\n' << "Etunimi: " << etunimi << '\n' << "Sukunimi: " << sukunimi << '\n' << "Vuosikurssi: " << vuosi << '\n';
    return ss.str();
}

    /**
    *  	\brief Opettaja clone
    */
henkilo* opettaja::clone() const{
    opettaja *op = new opettaja(etunimi,sukunimi,aine);
    return op;
}

    /**
    *  	\brief Oppilas clone
    */
henkilo* oppilas::clone() const{
    oppilas *op = new oppilas(etunimi,sukunimi,vuosi);
    return op;
}

    /**
    *  	\brief Koulun luominen
    */
koulu* opettaja::luoKoulu(){
    koulu *k1 = new koulu();
    return k1;
}


